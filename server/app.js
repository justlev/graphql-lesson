const express = require("express");
const graphqlHTTP = require("express-graphql");
const schema = require('./schema/schema');
const mongoose = require('mongoose');

const app = express();

mongoose.connect("mongodb://lev:lev123@ds251223.mlab.com:51223/graphql-test");
mongoose.connection.once('open', () => {
    console.log('connected to DB');
})

app.use("/graphql", graphqlHTTP({
    schema,
    graphiql: true
}));

app.listen(4000, () => {
    console.log("listening on 4000");
});
